#ifndef CHM_CPP
#define CHM_CPP

#include <thread>
#include <iostream>

#include "HashMapConcurrente.hpp"

HashMapConcurrente::HashMapConcurrente() {
    for (unsigned int i = 0; i < HashMapConcurrente::cantLetras; i++) {
        tabla[i] = new ListaAtomica<hashMapPair>();
    }
}

unsigned int HashMapConcurrente::hashIndex(std::string clave) {
    return (unsigned int) (clave[0] - 'a');
}

void HashMapConcurrente::incrementar(std::string clave) {
    // Ejercicio 2 (falta concurrencia)
    unsigned int claveIndex = hashIndex(clave);

    if (tabla[claveIndex] == nullptr) {         // si no existe la lista con esta letra, la creo
        mtx[claveIndex].lock();
        tabla[claveIndex] = new ListaAtomica<hashMapPair>();
        hashMapPair newPair = std::make_pair(clave, 1);
        tabla[claveIndex]->insertar(newPair);
        mtx[claveIndex].unlock();
        return;
    } else {
        for (auto it = tabla[claveIndex]->begin(); it != tabla[claveIndex]->end(); ++it) {
            if ((*it).first == clave) {
                mtx[claveIndex].lock();
                (*it).second++;
                mtx[claveIndex].unlock();
                return;
            }
        }
    }
    hashMapPair newPair = std::make_pair(clave, 1);
    mtx[claveIndex].lock();
    tabla[claveIndex]->insertar(newPair);
    mtx[claveIndex].unlock();
}

std::vector <std::string> HashMapConcurrente::claves() {
    // Ejercicio 2 (falta concurrencia)
    // Bloqueamos todas las listas y las vamos a ir liberando a medida que leemos las claves.
    for (int i = 0; i < 26; i++) {
        mtx[i].lock();
    }
    std::vector <std::string> todas_las_claves;
    for (int i = 0; i < cantLetras; i++) {          // voy a iterar por cada lista
        if (tabla[i] != nullptr) { // si esta lista no esta vacia...
            for (auto it = tabla[i]->begin(); it != tabla[i]->end(); ++it) {
                todas_las_claves.push_back((*it).first);    // pusheo cada clave
            }
        }
        mtx[i].unlock();
    }
    return todas_las_claves;
}

unsigned int HashMapConcurrente::valor(std::string clave) {
    // Ejercicio 2 (falta concurrencia)
    unsigned int hashIdx = hashIndex(clave);
    if (tabla[hashIdx] != nullptr) {
        for (auto it = tabla[hashIdx]->begin(); it != tabla[hashIdx]->end(); ++it) {
            if ((*it).first == clave) {
                return (*it).second;
            }
        }
    }
    return 0;
}

hashMapPair HashMapConcurrente::maximo() {
    hashMapPair *max = new hashMapPair();
    max->second = 0;

    for (unsigned int index = 0; index < HashMapConcurrente::cantLetras; index++) {
        for (auto &p: *tabla[index]) {
            if (p.second > max->second) {
                max->first = p.first;
                max->second = p.second;
            }
        }
    }

    return *max;
}

hashMapPair HashMapConcurrente::maximoParalelo(unsigned int cant_threads) {
    std::vector <std::thread> threads;
    //voy a crear 1 thread por letra para recorrer simultaneamente cada lista atomica
    std::vector < hashMapPair * > maxResults(cant_threads, new hashMapPair());
    //creo el vector que va a almacenar el resultado maximo de cada lista/letra

    std::atomic<int> maximumParallelIndex;
    maximumParallelIndex.store(0);

    for (unsigned int threadIndex = 0; threadIndex < cant_threads; threadIndex++) {
        std::thread thread([&](unsigned int threadIndex) {
            int index = maximumParallelIndex++;
            while (index < HashMapConcurrente::cantLetras) {
                for (auto &filaDeLetraI: *this->tabla[index]) {
                    if (filaDeLetraI.second > maxResults[threadIndex]->second) {
                        this->mtx[index].lock();
                        maxResults[threadIndex]->first = filaDeLetraI.first;
                        maxResults[threadIndex]->second = filaDeLetraI.second;
                        this->mtx[index].unlock();
                    }
                }
                index = maximumParallelIndex++;
            }
        }, threadIndex);
        threads.emplace_back(std::move(thread));
    }

    for (auto &thread: threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }

    hashMapPair finalMax = *maxResults[0];
    for (unsigned int i = 1; i < cant_threads; i++) {
        if (maxResults[i]->second > finalMax.second) {
            finalMax = *maxResults[i];
        }
    }

    return finalMax;
}

#endif
