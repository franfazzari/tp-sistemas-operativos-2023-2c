#ifndef CHM_CPP
#define CHM_CPP

#include <vector>
#include <iostream>
#include <fstream>
#include <thread>

#include "CargarArchivos.hpp"

int cargarArchivo(
        HashMapConcurrente &hashMap,
        std::string filePath
) {
    std::fstream file;
    int cant = 0;
    std::string palabraActual;

    // Abro el archivo.
    file.open(filePath, std::fstream::in);
    if (!file.is_open()) {
        std::cerr << "Error al abrir el archivo '" << filePath << "'" << std::endl;
        return -1;
    }
    while (file >> palabraActual) {
        // Completar (Ejercicio 4)
        hashMap.incrementar(palabraActual);
        cant++;
    }
    // Cierro el archivo.
    if (!file.eof()) {
        std::cerr << "Error al leer el archivo" << std::endl;
        file.close();
        return -1;
    }
    file.close();
    return cant;
}


void cargarMultiplesArchivos(
        HashMapConcurrente &hashMap,
        unsigned int cantThreads,
        std::vector<std::string> filePaths
) {
    // Ejercicio 4
    std::atomic<int> fileParallelIndex;
    fileParallelIndex.store(0);

    std::vector<std::thread> threads;
    for (unsigned int threadIndex = 0; threadIndex < cantThreads; threadIndex++) {
        std::thread thread([&hashMap, &filePaths, &fileParallelIndex]() {
            int index = fileParallelIndex++;
            while (index < filePaths.size()) {
                cargarArchivo(hashMap, filePaths[index]);
                index = fileParallelIndex++;
            }
        });
        threads.emplace_back(std::move(thread));
    }

    for (auto &thread: threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }
}

#endif
