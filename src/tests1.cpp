#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include "HashMapConcurrente.hpp"
#include "ListaAtomica.hpp"
#include "CargarArchivos.hpp"
#include <chrono>

using namespace std;

// Simple assert function to check if two values are equal
void assert(bool condition, const std::string &message) {
    if (!condition) {
        std::cerr << "Test failed: " << message << std::endl;
    }
}

void testNewMaximo() {
    // Test case: use incrementar to add words
    {
        //vector<string> file = {"hola", "mundo", "hola"};
        string folder_path = "."; // Assuming they are all in the same directory
        string fileName1 = "random_words.txt";
        HashMapConcurrente h;
        cargarArchivo(h, folder_path + "/" + fileName1);
        //for (const auto &word : file) {
        //    h.incrementar(word);
        //}

        struct timespec start, end;
        for (int i = 1; i < 26; i++){
            clock_gettime(CLOCK_REALTIME, &start);
            hashMapPair max_paraleloi = h.maximoParalelo(i);
            clock_gettime(CLOCK_REALTIME, &end);
            long tiempo_max_p = end.tv_nsec - start.tv_nsec;
            cout << "tiempo max paralelo con " << i << " threads: " << tiempo_max_p << '\n';

        }

        // In theory, they should all give the same result: "hola"
        clock_gettime(CLOCK_REALTIME, &start);
        hashMapPair max = h.maximo();
        cout << "el maximo es: " << max.first << " " << max.second << endl;
        clock_gettime(CLOCK_REALTIME, &end);
        long tiempo_max = end.tv_nsec - start.tv_nsec;
        cout << "tiempo max: " << tiempo_max << '\n';


        hashMapPair max_paralelo2 = h.maximoParalelo(2);
        cout << "el maximo con 2 threads es: "<< max_paralelo2.first << " " << max_paralelo2.second << endl;
        hashMapPair max_paralelo3 = h.maximoParalelo(3);
        hashMapPair max_paralelo4 = h.maximoParalelo(4);
        clock_gettime(CLOCK_REALTIME, &start);
        hashMapPair max_paralelo5 = h.maximoParalelo(10);
        clock_gettime(CLOCK_REALTIME, &end);
        cout << "el maximo con 5 threads es: "<< max_paralelo5.first << " " << max_paralelo5.second << endl;

        long tiempo_max_p = end.tv_nsec - start.tv_nsec;
        cout << "tiempo max paralelo: " << tiempo_max_p << '\n';

        assert(max == max_paralelo2, "max != max_p2 antes de incrementar");
        assert(max == max_paralelo3, "max != max_p3 antes de incrementar");
        assert(max == max_paralelo4, "max != max_p4 antes de incrementar");
        assert(max == max_paralelo5, "max != max_p5 antes de incrementar");

        h.incrementar("mundo");
        h.incrementar("mundo");

        // Now, all should give "mundo" as the new maximum

        max = h.maximo();
        cout << endl;
        cout << endl;
        cout << endl;
        cout << "el maximo despues de incrementar es: " << max.first << " " << max.second << endl;
        max_paralelo2 = h.maximoParalelo(2);
        cout << "el maximo con 2 threads despues de incrementar es: " << max_paralelo2.first << " " << max_paralelo2.second << endl;
        max_paralelo3 = h.maximoParalelo(3);
        cout << "el maximo con 3 threads despues de incrementar es: " << max_paralelo3.first << " " << max_paralelo3.second << endl;
        max_paralelo4 = h.maximoParalelo(4);
        max_paralelo5 = h.maximoParalelo(5);

        assert(max == max_paralelo2, "max != max_p2 despues de incrementar");
        assert(max == max_paralelo3, "max != max_p3 despues de incrementar");
        assert(max == max_paralelo4, "max != max_p4 despues de incrementar");
        assert(max == max_paralelo5, "max != max_p5 despues de incrementar");
    }
}




void testCargarArchivos() {
    // Test case: use cargarArchivo to load 3 files separately and cargarMultiplesArchivos to load them together. Both implementations of maximo should be the same
    {
        using namespace std::chrono;

        HashMapConcurrente hashMap1;
        HashMapConcurrente hashMap2;
        string folder_path = "."; // Assuming they are all in the same directory
        string fileName1 = "test-1";
        string fileName2 = "test-2"; // The file name was missing the hyphen
        string fileName3 = "test-3";

        auto start = high_resolution_clock::now();
        cargarArchivo(hashMap1, folder_path + "/" + fileName1);
        cargarArchivo(hashMap1, folder_path + "/" + fileName2);
        cargarArchivo(hashMap1, folder_path + "/" + fileName3);
        auto end = high_resolution_clock::now();
        auto tiempo_cargarArchivo = duration_cast<milliseconds>(end - start).count();
        cout << "tiempo cargar Archivo: " << tiempo_cargarArchivo << "ms\n";

        string filePath1 = folder_path + "/" + fileName1;
        string filePath2 = folder_path + "/" + fileName2;
        string filePath3 = folder_path + "/" + fileName3;
        vector<string> files = {filePath1, filePath2, filePath3};
        start = high_resolution_clock::now();
        cargarMultiplesArchivos(hashMap2, 3, files);
        end = high_resolution_clock::now();

        auto tiempo_cargarMultArchivo = duration_cast<milliseconds>(end - start).count();
        cout << "tiempo cargar multiples archivos: " << tiempo_cargarMultArchivo << "ms\n";

        hashMapPair max = hashMap1.maximo();
        hashMapPair max_paralelo = hashMap1.maximoParalelo(3);

        assert(max == max_paralelo, "max = max_paralelo");
    }
}



int main() {
    testNewMaximo();
    testCargarArchivos();
    return 0;
}
